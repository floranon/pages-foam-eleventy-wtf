let base_dir = 'hiker://files/rules/lw/zyw/';
if(!getVar('zyw.base_dir')){
    putVar('zyw.base_dir',base_dir);
}
function updata() {
    var res = {};
    var items = [];

    res.data = items;
    setHomeResult(res);
};

function filter(key) {
    //var word = JSON.parse(base64Decode('WyLkvKbnkIYiLCAi5YaZ55yfIiwgIuemj+WIqSIsICJWSVAiLCAi576O5aWzIiwgIumHjOeVqiIsICLmgKfmhJ8iLCAi5YCr55CGIiwgIuiuuueQhiIsICLmiJDkuroiLCAi5oOF6ImyIiwgIuaXoOeggSIsICLmnInnoIEiLCAi5aa7IiwgIuivsSIsICLkubMiLCAi57qi5Li7IiwgIuiOiSIsICLlk4HmjqgiLCAi5paH5a2XIiwgIuS4iee6pyIsICLnvo7lsJEiLCAiSEVZIiwgIumqkeWFtSIsICLkuqfoh6oiLCAi5oCn54ixIiwgIuijuOiBiiIsICLkubHkvKYiLCAi5YG3IiwgIkFWIiwgImF2IiwgIua3qyIsICLlppYiLCAi5ZCM5oCnIiwgIueUt+WQjCIsICLlpbPlkIwiLCAi5Lq6IiwgIuWmhyIsICLkuJ0iLCAi56eBIiwgIueblyIsICLomZrmi58iLCAi5LqkIiwgIlNNIiwgIuaFsCIsICLnsr7lk4EiLCAi5a2m55SfIiwgIuWwhCIsICIzUCIsICLlpKfnp4AiLCAi57K+5ZOBIiwgIuWPo+WRsyIsICLpq5jmva4iLCAi5p6B5ZOBIiwgIkRNTSIsICLpppbmrKEiLCAi6L6j5qSSIiwgIuWutuaTgiIsICLoibLmg4UiLCAi5Li75pKtIiwgIuWQjeS8mCIsICLlubwiLCAi55yJIiwgIuWlsyIsICLpmLQiLCAi5aW4IiwgIui9qCIsICLluIgiLCAi5oOF5L6jIiwgIua/gCIsICLmgIEiLCAi5o6nIiwgIumjnuacuiIsICLmjqgiLCAi5r2uIiwgIum6u+ixhiIsICJleSJd'));
    var word=[];
    for (var i = 0; i < word.length; i++) {
        if (key.indexOf(word[i]) > -1) {
            return true;
        }
    }
    return false;
};

function zywhm() {
    var html = getResCode();
    var arr = html.indexOf('http') != -1 ? html.match(/#[\s\S]*?#/g) : base64Decode(html).match(/#[\s\S]*?#/g);
    var setjson = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {}));
    var ssmd = setjson.ssmode;
    var ssxc = setjson.sscount;
    var self = JSON.parse(getRule()).title;
    var res = {};
    var items = [];
    var random = true;
    function randomSort3(arr) {
    arr.sort(function(a, b) {
        return Math.random() - 0.5;
    });
    return arr;
}
    //items.push({col_type: 'line'});

    var decText = getVar("xyqtext", "");
    items.push({
        title: '搜索',
        desc: "请输入搜索关键词",
        extra: {
            onChange: "putVar('xyqtext',input)",
            titleVisible: true
        },
        url:$.toString((self) => {
            return $("#noLoading#").lazyRule(rule => 'hiker://search?s='+ getVar('xyqtext') + '&rule='+rule, self);
        },self),
        col_type: "input"
    });

    var len = [];
    for (var i = 0; i < arr.length; i++) {
        var tabs = arr[i].match(/#.*?[\s]/g)[0].split('#')[1].replace(/\n/, '');
        var list = arr[i].match(/[\S]*?,.*?[\s]/g);
        items.push({
            title: tabs +'  '+'““(' + list.length + ')””',
            col_type: 'text_center_1'
        });
        if (random) {
        list = randomSort3(list)
    }
        for (var j = 0; j < list.length; j++) {
            len.push({
                title: list[j].split(',')[0]
            });
            let tmp_list = list[j].split(',');
            var title = tmp_list[0];
            var img = tmp_list.length >= 3 ? tmp_list[2].replace('\n', '') : '';
            var turl = tmp_list[1];
            items.push({
                title: list[j].split(',')[0],
                url: list[j].split(',')[1] + '?ac=list&pg=fypage',
                pic_url: img,
                col_type: 'icon_4'
            });
        }
    } //for arr.length

    items = items.concat([
        {col_type:'line_blank'},
        {title:'',col_type: 'text_center_1',url:'toast://道长占位专用，解决搜索框挡住下方按钮问题'}
    ]);
    
    let js_ver_info = {};
    try {
        let js_ver = JSON.parse(fetch('https://codeberg.org/Adn56/PublicRule/raw/branch/main/js/version.json',{}));
        if(js_ver.status===0){
            js_ver_info = {
                ver:js_ver.result.ver,
                publish_time:js_ver.result.last_update,
            }
            log('检测到云端插件版本:'+JSON.stringify(js_ver_info));
        }
    }catch (e) {
        log("检查插件版本出现错误:"+e.message);
    }
    let local_js_ver_info = JSON.parse(fetch(getVar('zyw.base_dir')+"requirements.json")||'{}');
    local_js_ver_info = local_js_ver_info['zywcj.js']||{};
    log('检测到本地插件版本:'+JSON.stringify(local_js_ver_info));
    let js_update_flag = "";
    if(local_js_ver_info.ver&&js_ver_info.ver){
        if(local_js_ver_info.ver!=js_ver_info.ver||local_js_ver_info.publish_time!=js_ver_info.publish_time){
            js_update_flag = '🆙';
        }
    }
    items.unshift({
        title: js_update_flag+'插件📥',
        url: $('hiker://empty').lazyRule((js_ver_info) => {
            let files = JSON.parse(getVar('zyw.files'));
            var rulejs = fetch(files['zywcj.js'], {});
            writeFile(getVar('zyw.base_dir')+"zywcj.js", rulejs);
            var ruleparse = fetch(files['parse.js'], {});
            writeFile(getVar('zyw.base_dir')+"parse.js", ruleparse);
            writeFile(getVar('zyw.base_dir')+"requirements.json", JSON.stringify({'zywcj.js':js_ver_info}));
            refreshPage(false);
            return 'toast://依赖插件[zywcj.js,parse.js]升级完毕。'
        },js_ver_info),
        col_type: 'scroll_button'
    });
    let dz_zyw = getVar('zyw.dz_zyw');
    let dz_zyw_ver = getVar('zyw.dz_zyw_ver');
    let files = JSON.parse(getVar('zyw.files'));
    let now_file = getVar('zyw.now_file');
    let now_file_ver = {};
    let local_file_ver = JSON.parse(fetch(getVar('zyw.base_dir')+"update.json")||'{}');
    if(files[now_file]&&files[now_file].indexOf(dz_zyw)>-1&&dz_zyw&&dz_zyw_ver){
        let update_info = files[now_file].replace(dz_zyw,dz_zyw_ver);
        try {
            let back = JSON.parse(fetch(update_info, {}))||{};
            if(back.status == 0){
                now_file_ver = back.result;
                let local_ver = local_file_ver[now_file]?parseInt(local_file_ver[now_file].ver):0;
                let online_ver = parseInt(now_file_ver.ver);
                let ver_title = local_ver+'';
                if(online_ver!=local_ver){
                    ver_title='🆙'+ver_title+'/'+online_ver;
                }
                items.unshift({
                    title: "版本:"+ver_title,
                    url:$("hiker://empty#noLoading#").rule(d=>setResult(d),
                        [
                            {title:'资源版本信息 '+now_file_ver.publish_time,desc:"云端版本: "+online_ver+'\n本地版本: '+local_ver+"\n本地更新时间: "+local_file_ver[now_file].publish_time,img:"",col_type:"text_1",url:"toast://没事儿别瞎鸡巴点！"}
                            ,{title:'插件版本信息 '+js_ver_info.publish_time,desc:"云端版本: "+js_ver_info.ver+'\n本地版本: '+local_js_ver_info.ver+"\n本地更新时间: "+local_js_ver_info.publish_time,img:"",col_type:"text_1",url:"toast://没事儿别瞎鸡巴点！"}
                        ]
                    ),
                    col_type: 'scroll_button'
                });
            }
        }
        catch (e) {
            log(e.message);
        }
    }

    if(files[now_file]) {
        items.unshift({
            title: '🔄资源' + '(共' + len.length + '个)',
            url: $('hiker://empty').lazyRule((now_file_ver, local_file_ver) => {
                let files = JSON.parse(getVar('zyw.files'));
                let now_file = getVar('zyw.now_file');
                var ruletxt = fetch(files[now_file], {});
                writeFile(getVar('zyw.base_dir') + now_file, ruletxt);
                local_file_ver[now_file] = now_file_ver;
                writeFile(getVar('zyw.base_dir') + "update.json", JSON.stringify(local_file_ver));
                refreshPage(false);
                return 'toast://应该是获取最新了吧。'
            }, now_file_ver, local_file_ver),
            col_type: 'scroll_button'
        });
    }
    items.unshift({
        title: '🔍设置' + '(' + (ssmd == 1 ? '聚' + ssxc : '列') + ')',
        url: $('hiker://empty').rule(() => {
            var d = [];
            var setjson = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {}));
            var ssmd = setjson.ssmode;
            var ssxc = setjson.sscount;
            d.push({
                title: '搜索模式设置',
                col_type: 'text_center_1'
            });
            d.push({
                title: '当前：' + '(' + (ssmd == 1 ? '聚合结果' : '引擎列表') + ')',
                url: $('hiker://empty').lazyRule(() => {
                    var md = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {})).ssmode;
                    if (md == 1) {
                        var fileUrl = fetch(getVar('zyw.base_dir')+"zywset.json", {}).replace('\"ssmode\":\"1\"', '\"ssmode\":\"0\"');
                        writeFile(getVar('zyw.base_dir')+"zywset.json", fileUrl);
                        back(true);
                        return 'toast://切换为搜索引擎列表单选模式成功！';
                    } else {
                        var fileUrl = fetch(getVar('zyw.base_dir')+"zywset.json", {}).replace('\"ssmode\":\"0\"', '\"ssmode\":\"1\"');
                        writeFile(getVar('zyw.base_dir')+"zywset.json", fileUrl);
                        back(true);
                        return 'toast://切换为聚合搜索模式成功！'
                    }
                }),
                col_type: 'text_2'
            })

            d.push({
                title: '搜索线程设置',
                col_type: 'text_center_1'
            });
            d.push({
                title: '当前线程' + ssxc + '  ' + '你输入的是' + parseInt(getVar('zywssxcset', '')),
                col_type: 'rich_text'
            });

            d.push({
                title: '☢️搜索线程',
                url: "input://" + '' + "////请输入一个整数数字，推荐最大不要超过15。.js:putVar('zywssxcset',input);refreshPage()",
                col_type: 'flex_button'
            });

            d.push({
                title: '🍀保存',
                url: $().lazyRule(() => {
                    var num = parseInt(getVar('zywssxcset')).toString();
                    if (num == 'NaN') {
                        return 'toast://输入的值好像不正确。';
                    } else {
                        var fileUrl = fetch(getVar('zyw.base_dir')+"zywset.json", {}).replace(/\"sscount\":\"[\S]*\"/, '\"sscount\":\"' + num + '\"');
                        writeFile(getVar('zyw.base_dir')+"zywset.json", fileUrl);
                        refreshPage(true);
                        return 'toast://保存设置搜索线程完成！';
                    }
                }),
                col_type: 'flex_button'
            });

            d.push({
                title: '🕒搜索超时['+getItem("zyw.search_timeout","5000")+']',
                url: $(getItem("zyw.search_timeout","5000"), "输入搜索返回超时[毫秒]\n最低1000").input(() => {
                    let now_set = (parseInt(input)!=0&&!parseInt(input))?'5000':input;
                    if(parseInt(now_set)<1000){
                        now_set = '1000';
                    }
                    setItem('zyw.search_timeout', now_set);
                    // let tips = '已设置搜索超时为:'+Math.ceil(parseFloat(now_set)/1000)+'秒';
                    let tips = '已设置搜索超时为:'+now_set+'豪秒';
                    refreshPage(true);
                    return 'toast://'+tips;
                }),
                col_type: 'flex_button'
            });

            d.push({
                title: '接口文件管理(支持xml与json采集接口)',
                col_type: 'text_center_1'
            });
            d.push({
                title: '打开编辑',
                url: $().lazyRule(() => {
                    return 'editFile://'+getVar('zyw.base_dir')+getVar('zyw.now_file');
                }),
                col_type: 'text_2'
            });

            d.push({
                title: '编码/解码',
                url: $().lazyRule(() => {
                    let file_path = getVar('zyw.base_dir')+getVar('zyw.now_file');
                    let file_code = fetch(file_path)||"";
                    let mode = file_code.indexOf('http')>-1?'编码':'解码';
                    file_code = mode==='解码'?base64Decode(file_code):base64Encode(file_code);
                    writeFile(file_path, file_code);
                    return 'toast://'+mode+'成功。';
                }),
                col_type: 'text_2'
            });
            setResult(d)
        }),
        col_type: 'scroll_button'
        // col_type: 'flex_button'
    });

    res.data = items;
    setHomeResult(res);
};


//图片替换函数
function picfun() {
    if (MY_URL.indexOf("pangniaozyw") != -1 || MY_URL.indexOf("leshizyw") != -1 || MY_URL.indexOf("9191zy") != -1) {
        pic = "https://tu.tianzuida.com/pic/" + pic;
    } else if (MY_URL.indexOf("lby") != -1) {
        pic.indexOf("http") != -1 ? pic = pic : pic = "http://cj.lby.pet/" + pic;
    } else if (MY_URL.indexOf("xjiys") != -1) {
        pic = pic.replace("img.maccms.com", "xjiys.com");
    } else if (MY_URL.indexOf("shidian") != -1) {
        pic = pic.replace("img.maccms.com", "shidian.vip");
    } else if (MY_URL.indexOf("17kanju") != -1) {
        pic = pic.replace("img.maccms.com", "img.17kanju.com");
    } else if (MY_URL.indexOf("potatost") != -1) {
        pic = pic.replace("http://img.maccms.com//pic=", "");
    }
};
//获取图片主页地址函数
function pichost(pic_url){
    let host=pic_url.split("//")[0]+"//"+pic_url.split("//")[1].split("/")[0];
//return encodeURIComponent(host);
    return host;
}

// 定义字符串替换全部函数
String.prototype.replaceAll = function(old,new_str){
    return this.split(old).join(new_str);
};

//列表解析函数
function listfun() {
    try {
        var list = parseDomForArray(html, "rss&&video");
        for (var j = 0; j < list.length; j++) {
            var title = parseDomForHtml(list[j], "body&&name&&Text").split('<')[0];
            var url = parseDomForHtml(list[j], "body&&id&&Text");
            var note = parseDomForHtml(list[j], "body&&note&&Text");
            var typ = parseDomForHtml(list[j], "body&&type&&Text");
            var last = parseDomForHtml(list[j], "body&&last&&Text");
            if (!filter(typ)) {
                if (html.indexOf("</pic>") != -1) {
                    var pic = parseDomForHtml(list[j], "body&&pic&&Text").replace("http://t.8kmm.com", "https://www.wxtv.net");
                    eval(fetch(getVar('zyw.base_dir')+"zywcj.js"));
                    picfun();
                    items.push({
                        title: title,
                        pic_url: pic + '@Referer=' + pichost(pic),
                        desc: note,
                        url: arrr + "?ac=videolist&ids=" + url +
                            `@rule=js:eval(fetch('`+getVar('zyw.base_dir')+`zywcj.js'));SSEJ();`,
                        col_type: "movie_3"
                    });
                } else {
                    var dt = parseDomForHtml(list[j], "body&&dt&&Text");
                    items.push({
                        title: title + "  状态:" + note,
                        desc: last + ' ' + typ + ' ' + dt,
                        url: arrr + "?ac=videolist&ids=" + url +
                            `@rule=js:eval(fetch('`+getVar('zyw.base_dir')+`zywcj.js'));SSEJ();`,
                        col_type: "text_1"
                    })
                }
            }
        }
    } catch (e) {}
};

//json列表解析函数
function jsonlist() {
    try {
        if (html.data) {
            var list = html.data;
        } else {
            var list = html.list;
        }
        for (var j = 0; j < list.length; j++) {
            var title = list[j].vod_name;
            var url = list[j].vod_id;
            var note = list[j].vod_remarks?list[j].vod_remarks:list[j].vod_total;
            var typ = list[j].type_name;
            var last = list[j].vod_addtime?list[j].vod_addtime:list[j].vod_time;
            if (!filter(typ)) {
                if (list[j].vod_pic) {
                    var pic = list[j].vod_pic;
                    items.push({
                        title: title,
                        pic_url: pic + '@Referer=' + pichost(pic),
                        desc: note,
                        url: arrr + "?ac=videolist&ids=" + url +
                            `@rule=js:eval(fetch('`+getVar('zyw.base_dir')+`zywcj.js'));SSEJ();`,
                        col_type: "movie_3"
                    });
                } else {
                    var dt = list[j].vod_play_from;
                    items.push({
                        title: title + "  状态:" + note,
                        desc: last + ' ' + typ + ' ' + dt,
                        url: arrr + "?ac=videolist&ids=" + url +
                            `@rule=js:eval(fetch('`+getVar('zyw.base_dir')+`zywcj.js'));SSEJ();`,
                        col_type: "text_1"
                    })
                }
            }
        }
    } catch (e) {}
};


//二级规则函数
function TWEJ() {
    var res = {};
    var items = [];
    var arrr = MY_URL.split("?")[0];
    var pn = MY_URL.split("=")[2];
    pn=pn.split('?')[0].replaceAll(' ','').replaceAll('\n','');
    var listmod = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {})).listmod;
    if (listmod == '1') {
        html = getResCode();
    } else {
        html = request(MY_URL.replace('ac=list', 'ac=videolist'))
    }
    //对第一页分类进行处理
    if (pn == '1') {
        try {
            if (/\<\/class\>/.test(html)) {
                rescod = getResCode();
            } else if (/type_name/.test(html)) {
                rescod = getResCode();
            } else {
                rescod = request(arrr + "?ac=list")
            }
            if (/list_name/.test(rescod)) {
                var type = JSON.parse(rescod).list;
            } else if (/type_name/.test(rescod)) {
                var type = JSON.parse(rescod).class;
            } else {
                var type = parseDomForHtml(rescod, "class&&Html").match(/<ty[\s]id[\s\S]*?<\/ty>/g);
            }
            log('分类列表:'+type);
            for (var i = 0; i < type.length; i++) {
                if (/list_name/.test(rescod)) {
                    var typ = type[i].list_name;
                    var tyid = type[i].list_id;
                } else if (/vod_play_from/.test(rescod)) {
                    var typ = type[i].type_name;
                    var tyid = type[i].type_id;
                } else {
                    var typ = parseDomForHtml(type[i], "body&&Text").split('{')[0];
                    var tyid = parseDomForHtml(type[i], "body&&ty&&id");
                }
                if (!filter(typ)) {
                    items.push({
                        title: typ,
                        url: $(arrr + "?ac=list&pg=fypage&t=" + tyid).rule(() => {
                            var arrr = MY_URL.split("?")[0];
                            var pn = MY_URL.split("pg=")[1].split("&t=")[0];
                            var listmod = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {})).listmod;
                            if (listmod == '1') {
                                html = getResCode();
                            } else {
                                html = request(MY_URL.replace('ac=list', 'ac=videolist'))
                            }
                            var res = {};
                            var items = [];
                            if (pn == '1') {
                                items.push({
                                    title: '‘‘’’<strong><font color="#ffaa64">纯文本列表</front></strong>',
                                    desc: '',
                                    url: $('hiker://empty').lazyRule(() => {
                                        var fileUrl = fetch(getVar('zyw.base_dir')+"zywset.json", {}).replace('\"listmod\":\"0\"', '\"listmod\":\"1\"');
                                        writeFile(getVar('zyw.base_dir')+"zywset.json", fileUrl);
                                        refreshPage();
                                        return 'toast://切换成功！'
                                    }),
                                    col_type: 'text_2'
                                });
                                items.push({
                                    title: '‘‘’’<strong><font color="#ffaa64">图文列表</front></strong>',
                                    desc: '',
                                    url: $('hiker://empty').lazyRule(() => {
                                        var fileUrl = fetch(getVar('zyw.base_dir')+"zywset.json", {}).replace('\"listmod\":\"1\"', '\"listmod\":\"0\"');
                                        writeFile(getVar('zyw.base_dir')+"zywset.json", fileUrl);
                                        refreshPage();
                                        return 'toast://切换成功！'
                                    }),
                                    col_type: 'text_2'
                                });
                                items.push({
                                    col_type: 'line'
                                });
                            }

                            if (/vod_play_from/.test(html)) {
                                html = JSON.parse(html);
                                eval(fetch(getVar('zyw.base_dir')+'zywcj.js'));
                                jsonlist();
                            } else {
                                eval(fetch(getVar('zyw.base_dir')+'zywcj.js'));
                                listfun();
                            }

                            res.data = items;
                            setHomeResult(res);
                        }),
                        //col_type:"text_3"
                        col_type: type.length >= 16 ? 'scroll_button' : 'flex_button'
                        //col_type:'flex_button'
                    });
                }
            }
        } catch (e) {
            log('设置分类以及模式切换按钮出错!'+e.message);
        }
        items.push({
            col_type: 'line'
        });
        items.push({
            title: '‘‘’’<strong><font color="#ffaa64">纯文本列表</front></strong>',
            desc: '',
            url: $('hiker://empty').lazyRule(() => {
                var fileUrl = fetch(getVar('zyw.base_dir')+"zywset.json", {}).replace('\"listmod\":\"0\"', '\"listmod\":\"1\"');
                writeFile(getVar('zyw.base_dir')+"zywset.json", fileUrl);
                refreshPage();
                return 'toast://切换成功！'
            }),
            col_type: 'text_2'
        });
        items.push({
            title: '‘‘’’<strong><font color="#ffaa64">图文列表</front></strong>',
            desc: '',
            url: $('hiker://empty').lazyRule(() => {
                var fileUrl = fetch(getVar('zyw.base_dir')+"zywset.json", {}).replace('\"listmod\":\"1\"', '\"listmod\":\"0\"');
                writeFile(getVar('zyw.base_dir')+"zywset.json", fileUrl);
                refreshPage();
                return 'toast://切换成功！'
            }),
            col_type: 'text_2'
        });
        items.push({
            col_type: 'line'
        });
    }
    //结束第一页分类处理

    //对列表处理开始
    if (/vod_play_from/.test(html)) {
        html = JSON.parse(html);
        eval(fetch(getVar('zyw.base_dir')+'zywcj.js'));
        jsonlist();
    } else {
        eval(fetch(getVar('zyw.base_dir')+'zywcj.js'));
        listfun();
    }
    //对列表处理结束
    res.data = items;
    setHomeResult(res);
};

function zywsea() {
    var res = {};
    var items = [];
    //获取搜索线程数量
    var ssxc = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {})).sscount;

    //设置超时时间，越小越快，单位毫秒
    // var timeou = 3000;
    var timeou = parseInt(getItem("zyw.search_timeout","5000"));

    var ss = MY_URL.split('$$$')[1];
    var num = MY_URL.split('$$$')[2];
    var ssmode = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {})).ssmode;
    var le = num * ssxc;
    let now_file = getVar('zyw.base_dir')+(getVar('zyw.now_file')||"成人资源.txt");
    log('当前搜索文件:'+now_file);
    var src = fetch(now_file, {});
    //var src = fetch('', {});
    //打乱顺序
    function randomSort3(arr) {
    arr.sort(function(a, b) {
        return Math.random() - 0.5;
    });
    return arr;
}
    var random = true;
    var cc = src.indexOf('http') != -1 ? src.match(/[\S]*?,.*?[\s]/g) : base64Decode(src).match(/[\S]*?,.*?[\s]/g);
    if(random){
        cc = randomSort3(cc)
    };
    if (ssmode == '0') {
        for (var i = 0; i < cc.length; i++) {
            //屏蔽不支持搜索的1717和穿梭
            if (!/itono|888hyk/.test(cc[i])) {
                items.push({
                    title: cc[i].split(',')[0] + ' 点击查看搜索结果',
                    url: cc[i].split(',')[1] + ss.replace(/pg=\d*/g, 'pg=fypage') + `@rule=js:var erj=fetch("`+getVar('zyw.base_dir')+`zywcj.js",{});eval(erj);zywerj();`,
                    col_type: 'text_center_1'
                });
            }
        };
    } else {
        var Data = [];
        var Tit = [];
        var Ost = [];
        for (var i = le - ssxc; i < le; i++) {
            if (i < cc.length) {
                var arr = cc[i].split(',')[1];
                var arrt = cc[i].split(',')[0];
                var link = cc[i].split(',')[1].replace('\n','').replace('\r','') + ss;
                //屏蔽不支持搜索的1717和穿梭
                if (!/itono|888hyk/.test(cc[i])) {
                    //Data.push({url:link,options:{headers:{'User-Agent':MOBILE_UA},timeout:timeou}});
                    Data.push({
                        url: link,
                        options: {
                            headers: {
                                'User-Agent': MOBILE_UA
                            },
                            timeout: timeou
                        }
                    });
                    Tit.push({
                        tit: arrt
                    });
                    Ost.push({
                        url: arr
                    });
                }
            }
        }
        //---------------代码分界线----------------

        //批量发送请求
        if (Data != '') {
            var bfhtml = batchFetch(Data);
            //setError(Tit);
            for (var k = 0; k < bfhtml.length; k++) {
                var html = bfhtml[k];
                //搜索结果网页处理开始
                if (/\<video\>/.test(html) || /vod_name/.test(html)) {
                    if (/list_name/.test(html)) {
                        var list = JSON.parse(html).data;
                    } else if (/vod_name/.test(html)) {
                        var list = JSON.parse(html).list;
                    } else {
                        var list = parseDomForArray(html, 'rss&&video');
                    }
                    //setError(list[0]);
                    for (var j = 0; j < list.length; j++) {
                        if (/vod_name/.test(html)) {
                            var title = list[j].vod_name;
                            var ids = list[j].vod_id;
                            var note = /vod_remarks/.test(html)?list[j].vod_remarks:list[j].vod_total;
                            var typ = list[j].type_name;
                            var dt = list[j].vod_play_from;
                        } else {
                            var title = parseDomForHtml(list[j], 'body&&name&&Text');
                            var ids = parseDomForHtml(list[j], 'body&&id&&Text');
                            var note = parseDomForHtml(list[j], 'body&&note&&Text');
                            var typ = parseDomForHtml(list[j], 'body&&type&&Text');
                            var dt = parseDomForHtml(list[j], 'body&&dt&&Text');
                        }
                        items.push({
                            title: title + " " + ' • ' + note,
                            desc: ' ' + Tit[k].tit + ' · ' + typ + ' · ' + dt,
                            url: Ost[k].url + "?ac=videolist&ids=" + ids + `@rule=js:var erj=fetch("`+getVar('zyw.base_dir')+`zywcj.js",{});eval(erj);SSEJ();`,
                            col_type: 'text_center_1'
                        });

                    } //for j
                } else {
                    items.push({
                        title: '““' + Tit[k].tit + '””' + '未搜索到相关资源',
                        url: Data[k].url + `@lazyRule=.js:input+'#ignoreVideo=true#'`,
                        col_type: 'text_center_1'
                    });
                }
            } //for k
        } //if Data

    } //聚/列

    res.data = items;
    setSearchResult(res);
};

function zywerj() {
    var ssmode = JSON.parse(fetch(getVar('zyw.base_dir')+'zywset.json', {})).ssmode;
    if (ssmode == '0') {
        var res = {};
        var items = [];
        var domain = MY_URL.split('?wd')[0];
        var html = getResCode();

        //setError(domain);
        if (/\<video\>/.test(html) || /vod_name/.test(html)) {
            if (/list_name/.test(html)) {
                var list = JSON.parse(html).data;
            } else if (/vod_name/.test(html)) {
                var list = JSON.parse(html).list;
            } else {
                var list = parseDomForArray(html, 'rss&&video');
            }
            for (var j = 0; j < list.length; j++) {
                if (/vod_name/.test(html)) {
                    var title = list[j].vod_name;
                    var ids = list[j].vod_id;
                    var note = /vod_remarks/.test(html)?list[j].vod_remarks:list[j].vod_total;
                    var last = /vod_addtime/.test(html)?list[j].vod_addtime:list[j].vod_time;
                    var typ = list[j].type_name;
                    var dt = list[j].vod_play_from;
                } else {
                    var title = parseDomForHtml(list[j], 'body&&name&&Text');
                    var ids = parseDomForHtml(list[j], 'body&&id&&Text');
                    var note = parseDomForHtml(list[j], 'body&&note&&Text');
                    var last = parseDomForHtml(list[j], "body&&last&&Text");
                    var typ = parseDomForHtml(list[j], 'body&&type&&Text');
                    var dt = parseDomForHtml(list[j], 'body&&dt&&Text');
                }
                items.push({
                    title: '““' + title + '””' + " " + ' • ' + note,
                    desc: last + ' ·  ' + typ + ' ·  ' + dt,
                    url: domain + "?ac=videolist&ids=" + ids + `@rule=js:var erj=fetch("`+getVar('zyw.base_dir')+`zywcj.js",{});eval(erj);SSEJ();`,
                    col_type: 'text_center_1'
                });

            }
        } else {
            items.push({
                title: '未搜索到相关资源',
                url: MY_URL,
                col_type: 'text_center_1'
            });
        }

        res.data = items;
        setHomeResult(res);
    } else {

        var net = fetch(getVar('zyw.base_dir')+'zywcj.js', {});
        eval(net);
        SSEJ();
    }
};


//选集与简介规则
function SSEJ() {
    var res = {};
    var items = [];
    refreshX5WebView("");
    items.push({
        title: '',
        desc: '255&&float',
        url: '',
        col_type: 'x5_webview_single'
    });
    var html = getResCode().replace(/\<\!\[CDATA\[/g,'').replace(/\]\]\>/g,'');
    try {
        if (/vod_play_from/.test(html)) {
            var jhtml = JSON.parse(html);
            if (/list_name/.test(html)) {
                var pic = jhtml.data[0].vod_pic;
                var typ = jhtml.data[0].type_name;
                var des = jhtml.data[0].vod_content;
                var act = jhtml.data[0].vod_actor;
                var dir = jhtml.data[0].vod_director;
                var tabs = jhtml.data[0].vod_play_from.split('$$$');
                var conts = jhtml.data[0].vod_play_url.split('$$$');
                var url = jhtml.data[0].vod_id;
            } else {
                var pic = jhtml.list[0].vod_pic;
                var typ = jhtml.list[0].type_name;
                var des = jhtml.list[0].vod_content;
                var act = jhtml.list[0].vod_actor;
                var dir = jhtml.list[0].vod_director;
                var tabs = jhtml.list[0].vod_play_from.split('$$$');
                var conts = jhtml.list[0].vod_play_url.split('$$$');
                var url = jhtml.list[0].vod_id;
            }
        } else {
            var pic = parseDomForHtml(html, "rss&&pic&&Text").replace("http://t.8kmm.com", "https://www.wxtv.net");
            eval(fetch(getVar('zyw.base_dir')+'zywcj.js'));
            picfun();
            var typ = parseDomForHtml(html, "body&&type&&Text");
            var des = parseDomForHtml(html, "rss&&des&&Text");
            var act = parseDomForHtml(html, "rss&&actor&&Text");
            var dir = parseDomForHtml(html, "rss&&director&&Text");
            var tabs = parseDomForArray(html, 'rss&&dl&&dd');
            var conts = parseDomForArray(html, 'rss&&dl&&dd');
            var url = parseDomForHtml(html, 'rss&&id&&Text');

        }
        //log(tabs);

        if (!filter(typ)) {
            items.push({
                title: '演员：' + '\n' + act,
                desc: '导演：' + dir,
                pic_url: pic + '@Referer=' + pichost(pic),
                url: pic,
                col_type: 'movie_1_vertical_pic'
            });

            items.push({
                title: "剧情简介：",
                desc: des,
                url: $('hiker://empty').rule((des) => {
                    var res = {};
                    var items = [];
                    items.push({
                        title: des,
                        col_type: 'long_text'
                    });
                    res.data = items;
                    setHomeResult(res);
                }, des),
                col_type: 'text_1'
            });
            //-----简介选集分割线---//
            for (var i = 0; i < conts.length; i++) {
                if (getVar('zywlsort', '1') == '1') {
                    if (/dd flag/.test(conts)) {
                        var list = conts[i].split(">\n")[1].split("\n<")[0].split("#");
                    } else if (/\r/.test(conts)) {
                        var list = conts[i].split("\r");
                    } else {
                        var list = conts[i].split("#");
                    }
                } else {
                    if (/dd flag/.test(conts)) {
                        var list = conts[i].split(">\n")[1].split("\n<")[0].split("#").reverse();
                    } else if (/\r/.test(conts)) {
                        var list = conts[i].split("\r").reverse();
                    } else {
                        var list = conts[i].split("#").reverse();
                    }
                }
                //log(list);
                if (/dd flag/.test(conts)) {
                    var flag = parseDomForHtml(tabs[i], "dd&&flag");
                } else {
                    var flag = tabs[i];
                }
                //如果列表不为null就显示选集
                if (list != null) {
                    items.push({
                         title: flag + "    🔗" + [i + 1] + '/' + conts.length + "““↓↑””",
                        
                        url: "hiker://empty@lazyRule=.js:putVar('zywlsort', getVar('zywlsort','1')=='1'?'0':'1');refreshPage(false);'toast://切换成功！'",
                        col_type: 'text_1'
                    });

                    var url = {};
                    for (var j = 0; j < list.length; j++) {
                        if (list[j].split('$')[1] != null) {
                            url = list[j].split('$')[1];
                        } else {
                            url = list[j].split('$')[0];
                        }
                        if (MY_URL.indexOf('leduo') != -1) {
                            url = 'https://api.xxctzy.com/wp-api/glid.php?vid=' + url
                        }
                        if (MY_URL.indexOf('bbkdj') != -1) {
                            url = 'http://jx.yparse.com/index.php?url=' + url
                        }
                        if (MY_URL.indexOf('7kjx') != -1) {
                            url = 'https://jx.xmflv.com/?url=' + url
                        }
                        if (MY_URL.indexOf('ujuba') != -1) {
                            url = 'https://zy.ujuba.com/play.php?url=' + url
                        }
                        if (flag == 'ddyunp' || flag == 'xin') {
                            url = 'https://player.90mm.me/play.php?url=' + url.replace(/第.*?集/g, '')
                        }
                        if (flag == 'qdyun') {
                            url = 'http://qdy.zt6a.cn/parse?resources=' + url
                        }
                        if (flag == 'ppayun' || flag == 'gangtiexia') {
                            url = url.substring(0, 4) == 'http' ? url.replace('683d2433ee134cde8063d50506c1a1b1', '3bb24322f78b47dfb8723c13d46d45ee') : 'https://wy.mlkioiy.cn/api/GetDownUrlWy/3bb24322f78b47dfb8723c13d46d45ee/' + url
                        }
                        if (flag == 'tt10') {
                            url = 'https://wy.mlkioiy.cn/api/ShowVideoMu/3bb24322f78b47dfb8723c13d46d45ee/' + url
                        }
                        if (MY_URL.indexOf('yyid6080') != -1 || MY_URL.indexOf('p4kan') != -1) {
                            if (flag == 'xigua' || flag == 'bjm3u8') {
                                url = 'https://bbs.cnzv.cc/dp/mp4.php?url=http://list.47api.cn:90/qq/xigua.php?id=' + url;
                            } else if (flag == 'qqkj') {
                                url = 'https://bbs.cnzv.cc/dp/ck/ck.php?url=http://list.47api.cn:90/qq/mp4.php?id=' + url;
                            } else if (flag == 'tudou') {
                                url = 'https://sf1-ttcdn-tos.pstatp.com/obj/' + url + '#.mp4';
                            } else {
                                url = url
                            };
                        }
                        //if(flag=='rrm3u8'){url='https://www.meiju11.com/ckplayerx/m3u8.php?url='+url}
                        if (flag == 'niux') {
                            url = 'https://www.shenma4480.com/jx.php?id=' + url
                        }
                        if (flag == 'hkm3u8') {
                            url = 'https://jxn2.178du.com/hls/' + url
                        }
                        if (flag == 'xsp1') {
                            url = 'https://jx.api.xhfhttc.cn/jx/?type=xsp1&url=' + url
                        }
                        if (flag == 'bb') {
                            url = 'https://jx.api.xhfhttc.cn/jx/?url=' + url
                        }
                        if (flag == 'pll') {
                            url = 'https://vip.gaotian.love/api/?key=sRy0QAq8hqXRlrEtrq&url=' + url
                        }
                        if (flag == 'languang') {
                            url = 'https://j.languang.wfss100.com/?url=' + url
                        }
                        if (flag == 'msp') {
                            url = 'https://titan.mgtv.com.bowang.tv/player/?url=' + url
                        }
                        if (flag == 'kdyx' || flag == 'kdsx') {
                            url = 'http://api.kudian6.com/jm/pdplayer.php?url=' + url
                        }
                        if (flag == '789pan') {
                            url = 'https://vip.gaotian.love/api/?key=GiML8kaI6DnlpAahLM&url=' + url
                        }
                        if (flag == 'fanqie') {
                            url = 'https://jx.fqzy.cc/jx.php?url=' + url
                        }
                        if (flag == 'mysp' || flag == 'xmzy' || flag == 'tyun') {
                            url = 'http://jiexi.sxmj.wang/jx.php?url=' + base64Encode(url)
                        }
                        if (flag == 'lekanzyw') {
                            url = 'https://bak.ojbkjx.com/?url=' + url
                        }
                        if (flag == 'renrenmi') {
                            url = 'https://cache1.jhdyw.vip:8091/rrmi.php?url=' + url
                        }
                        if (flag == 'yunbo') {
                            url = 'https://www.mayigq.com/vodzip/player.php?vid=' + url
                        }
                        if (flag == 'duoduozy') {
                            url = 'https://player.duoduozy.com/ddplay/?url=' + url
                        }
                        //if (flag == 'miaoparty') {
                        //url = 'https://jiexi.msdv.cn/jiemi/?url=' + url
                        //}
                        var title = (list[j].split('$')[0].indexOf('http') != -1 ? [j + 1] : list[j].split('$')[0]);
                        if (list.length <= 4) {
                            var clt = 'text_2';
                        } else {
                            var clt = isNaN(title) ? 'flex_button' : 'text_5'
                        }
                        // 854行有问题，filter(base64Decode('VklQ'))就是V I P
                        items.push({
                            title: list[j].split('$')[0].indexOf('http') != -1 ? [j + 1] : list[j].split('$')[0],
                            url: 'hiker://empty##' + flag + '##' + url.replace(/\n*/g, '') + '##' + `@lazyRule=.js:/*refreshX5WebView*/eval(fetch('`+getVar('zyw.base_dir')+`zywcj.js'));lazyRu();`,
                            //col_type: title.length>=6?'text_2':'text_3'
                            col_type: clt
                        });
                    } //for j list.length
                } //if list != null
            } //for i conts.length
        } //!filter(typ)
    } catch (e) {}
    res.data = items;
    setHomeResult(res);
};

//动态解析
function lazyRu() {
    var flag = input.split('##')[1];
    var src = (input.split('##')[2]).replace(/amp;/g, "").replace(/^\s*/, "");
    if (flag == 'qq' || flag == 'qiyi' || flag == 'youku' || flag == 'mgtv' || flag == 'letv' || flag == 'sohu' || flag == 'pptv' || flag == 'm1905') {
        var fileUrl = getVar('zyw.base_dir')+"parse.js";
        //var fileUrl = "hiker://files/rules/parse.js";
        eval(fetch(fileUrl, {}));
        var play = vodkey.toUrl(src.split('"')[0]);
        if (play != "" && play !== undefined) {
            return play;
        } else {
            var play = btoken.toUrl(src.split('"')[0]);
            if (play != "" && play !== undefined) {
                return play;
            } else {
                var play = yqjx.toUrl(src.split('"')[0]);
                if (play != "" && play !== undefined) {
                    return play;
                } else {
                    return 'http://17kyun.com/api.php?url=' + (src.split('"')[0]);
                }
            }
        }
    } else if (src.indexOf("xmflv") != -1) {
        eval(getCryptoJS());
        //感谢墙佬提供算法代码
        var sign = function(a) {
            var b = CryptoJS.MD5(a);
            var c = CryptoJS.enc.Utf8.parse(b);
            var d = CryptoJS.enc.Utf8.parse('ren163com5201314');
            var e = CryptoJS.AES.encrypt(a, c, {
                iv: d,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.ZeroPadding
            });
            return e.toString()
        }
        var html = fetch(src, {
            headers: {
                "User-Agent": MOBILE_UA,
                "Referer": "https://jx.xmflv.com"
            }
        });
        var svg = html.match(/\[\'post\'\]\(\'(.*?)\'/)[1];
        var time = html.match(/var time = \'(.*?)\'/)[1];
        var url = html.match(/var url = \'(.*?)\'/)[1];
        var vkey = html.match(/var vkey = \'(.*?)\'/)[1];
        var fvkey = sign(html.match(/var fvkey = \'(.*?)\'/)[1]);
        var body = 'time=' + time + '&url=' + url + '&wap=1&vkey=' + vkey + '&fvkey=' + fvkey;
        var json = fetch('https://jx.xmflv.com' + svg, {
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            body: body,
            method: 'POST'
        });
        //log(json);
        return JSON.parse(json).url;
    } else if (src.indexOf('obj/tos') != -1) {
        return src + '#isVideo=true#';
    } else if(/wkfile/.test(src)){
        return src+';{Referer@https://fantuan.tv}'
    }else if (src.indexOf("135-cdn") != -1) {
        refreshX5WebView(src);
        return "toast://请等待加载选集！";
    } else if (src.indexOf("/share/") != -1) {
        try {
            var link = src.split("/share")[0];
            var fc = fetch(src, {}).replace("var purl", "var main");
            if (fc.indexOf("main") != -1) {
                var mat = fc.match(/var main.*?;/)[0];
                eval(mat);
                var play = (main.indexOf("http") != -1 ? main : link + main);
            } else {
                var main = fc.match(/url:.*?[\'\"](.*?)[\'\"]/)[1];
                var play = (main.indexOf("http") != -1 ? main : link + main)
            };
            return play;
        } catch (e) {
            refreshX5WebView(src);
            return "toast://请等待加载选集！"
        };
    }
    /*else if(src.indexOf("meiju11")!=-1){
    var meiju=fetch(src,{headers:{"User-Agent":MOBILE_UA,"Referer":"https://www.meiju11.com"}});
    return meiju.match(/url:.*?[\'\"](.*?)[\'\"]/)[1];
    }*/
    else if(src.indexOf("duoduozy")!=-1){
        var duoduo=fetch(src,{headers:{"User-Agent":MOBILE_UA,"Referer":"https://www.duoduozy.com/"}});
        return duoduo.match(/var urls.*?[\'\"](.*?)[\'\"]/)[1];
    }
    else if(flag=='miaoparty'){
        var miao = fetch('https://cc-dd-112266-t.ms180.fun/偷接口死妈/api.php', {
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            body: 'url=' + src + '&ac=jx',
            method: 'POST'
        });
        return JSON.parse(miao).url;
    }
    else if (/fqzy\.cc|ojbkjx/.test(src)) {
        var html = fetch(src, {
            headers: {
                "User-Agent": MOBILE_UA
            }
        });
        if (/bt_token/.test(html)) {
            var fileUrl = fetch("https://cdn.jsdelivr.net/gh/lzk23559/Public_folder/token.js", {});
            eval(fileUrl);
            var play = (tkurl.indexOf('url=') > -1 ? tkurl.split('url=')[1] : tkurl);
        } else {
            var play = html.match(/\"url\": \"(.*?)\"/)[1]
        }
        if(/4kan/.test(play)){return play+';{Referer@https://bak.ojbkjx.com/}';}else{return play};
    } else if (src.indexOf("xxctzy") != -1) {
        var purl = request(src, {}).split("var url=\'")[1].split("\'")[0];
        var pla = request("https://api.xxctzy.com" + purl, {});
        return pla.match(/\"url\": \"(.*?)\"/)[1];
        //if(pla.indexOf("m3u8")!=-1){
        //return pla.split("=")[1];}else{return src};
    } else if (src.indexOf("aHR0c") != -1) {
        return decodeURIComponent(base64Decode(src.split("&")[0]));
    } else if (src.indexOf("haodanxia") != -1 || src.indexOf("cqzyw") != -1) {
        var ul = JSON.parse(fetch(src, {
            headers: {
                "User-Agent": "Dalvik/2.1.0"
            },
            redirect: false,
            withStatusCode: true
        }));
        if (ul.statusCode == "302") {
            var play = ul.headers.location[0];
        } else {
            var play = src + "#isVideo=true#"
        };
        return play;
    } else if (src.indexOf("shenma4480") != -1) {
        var sm = "https://www.shenma4480.com/" + fetch(src, {
            headers: {
                "User-Agent": MOBILE_UA,
                "Referer": "https://www.shenma4480.com"
            }
        }).match(/var u=\"(.*?)\"/)[1];
        return fetch(sm, {
            headers: {
                "User-Agent": MOBILE_UA,
                "Referer": "https://www.shenma4480.com"
            }
        }).match(/url:.*?[\'\"](.*?)[\'\"]/)[1].replace(/[+]/g, "%20");
    } else if (src.indexOf("mlkioiy") != -1) {
        if (src.indexOf("ShowVideo") != -1) {
            var mlki = parseDomForHtml(fetch(src, {}), "body&&#dplayer&&result");
            var fileUrl = "https://cdn.jsdelivr.net/gh/lzk23559/rulehouse/pako-min.js";
            eval(request(fileUrl, {}));
            return realUrl;
        } else {
            return src + "#isVideo=true#"
        };
    } else if (src.indexOf("ddyunp") != -1 || src.indexOf("90mm.me") != -1) {
        eval(getCryptoJS());
        var id = src + 'duoduo' + 'l' + (Math.floor(new Date().getTime() / 100000) * 100).toString();
        var dat = CryptoJS.MD5(id).toString(CryptoJS.enc.Hex);
        var purl = 'https://hls.90mm.me/ddyun/' + src + '/l/' + dat + '/playlist.m3u8';
        return purl;
    } else if (src.indexOf("xsp1") != -1) {
        var pli = parseDomForHtml(fetch(src, {
            headers: {
                "User-Agent": MOBILE_UA,
                "Referer": "https://zz22x.com"
            }
        }), "body&&iframe&&src").split("url=")[1];
        var fileUrl = getVar('zyw.base_dir')+"parse.js";
        eval(fetch(fileUrl, {}));
        var play = yqjx.toUrl(pli);
        return play != "" ? play : getUrl(pli);
    } else if (src.indexOf("kudian6.com") != -1) {
        var html = request(src);
        return html.match(/url\":.*?[\'\"](.*?)[\'\"]/)[1];
    } else if (/719ns|rrmi/.test(src)) {
        /*
var html=request(src);
eval(getCryptoJS());
var id = html.match(/var id=\"(.*?)\"/)[1];
var times=(new Date()).getTime()+'';
var sh= CryptoJS.MD5(base64Encode(id+times)).toString();
var purl='http://play.zk132.cn/new/play1/'+id+'%7C'+times+'%7C'+sh+'%7C'+'1'+'%7C'+'index.m3u8';
*/
        var html = fetch(src, {});
        return 'x5Play://' + JSON.parse(html).url;
    } else if (/wfss100/.test(src)) {
        var phtml = request(src, {});
        var ifsrc = src.split('/?url=')[0] + parseDomForHtml(phtml, "body&&iframe&&src");
        var ifsrct = ifsrc.split('?url=')[0] + parseDomForHtml(request(ifsrc, {}), "body&&iframe&&src");
        var urll = request(ifsrct, {}).match(/vodurl = \'(.*?)\'/)[1];
        return urll + ';{Referer@https://j.languang.wfss100.com/}';
    } else if (flag == 'pll'||flag == '789pan') {
        var json = JSON.parse(fetch(src, {}));
        if (json.code == '200') {
            return json.url;
        }
    } else if (flag == 'bilibili') {
        //var zxyb = fetch('https://www.zxyb.cc/bd_json.php?url=' + src, {});
        //return JSON.parse(zxyb).url + ';{Referer@https://www.bilibili.com&&User-Agent@Mozilla/5.0}';
        return 'http://17kyun.com/api.php?url='+src;
    } else if (src.indexOf("alizy-") != -1) {
        refreshX5WebView('http://hong.1ren.ren/?url=' + src);
        return "toast://请等待加载选集！";
    } else if (src.indexOf("47api") != -1) {
        refreshX5WebView(src);
        return "toast://请等待加载选集！";
    } else if (src.indexOf("yparse.com") != -1) {
        refreshX5WebView(src);
        return "toast://请等待加载选集！";
    } else if (src.indexOf("//520.com") != -1) {
        refreshX5WebView("https://titan.mgtv.com.o8tv.com/jiexi/?url=" + src);
        return "toast://请等待加载选集！";
    } else {
        return src
    }
};


//预处理代码
function zywpre() {
    if (!fetch(getVar('zyw.base_dir')+'zywset.json', {})) {
        var set = `{"ssmode":"1","listmod":"0","sscount":"10"}`;
        writeFile(getVar('zyw.base_dir')+"zywset.json", set);
    }

}